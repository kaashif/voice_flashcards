import speech_recognition as sr
import json, random, os.path

def main():
    full_phrases = load_phrases()
    number_of_phrases = query_number()
    
    for flashcard,answer in gen_cards(full_phrases, number_of_phrases):
        print("{}: ".format(flashcard), end="")
        you_said = listen_for_phrase()
        correct = "correct" if answer == you_said else "incorrect"
        print("{}: {}".format(you_said, correct))

    print("Done!")

def load_phrases(phrasefile="~/.voice_flashcards.json"):
    pfile = open(os.path.expanduser(phrasefile), "r")
    return json.load(pfile)

def query_number():
    num = 0
    try:
        num = int(input("How many flashcards? "))
    except ValueError:
        pass
    return num

def gen_cards(full, num):
    phrases = full.copy()
    if not num <= len(phrases):
        raise IndexError("can't provide that many flashcards!")
    for _ in range(0,num):
        rkey = random.choice(list(phrases.keys()))
        rpair = (rkey, phrases[rkey])
        del phrases[rkey]
        yield rpair

def listen_for_phrase():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        audio = r.listen(source)
    try:
        return r.recognize_google(audio, language = "fr-FR")
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
    return "error"

    

#!/usr/bin/env python3
import os
from setuptools import setup

setup(
    name = "voice_flashcards",
    version = "0.0.1",
    author = "kaashif",
    description = ("Voice flashcard program"),
    license = "BSD",
    packages = ["voice_flashcards"],
    scripts = ["scripts/voice_flashcards"],
    install_requires = [
        "SpeechRecognition",
        "PyAudio"
    ]
)
